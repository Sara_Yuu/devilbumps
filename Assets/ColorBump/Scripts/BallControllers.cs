using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallControllers : MonoBehaviour
{
    Rigidbody rb;
    Vector2 lastMousePos = Vector2.zero; //initial position of the ball
    public float ballThrust = 100f;
    public float movingSpeed = 5f;
    [SerializeField] float wallDistance = 5f;
    [SerializeField] float minCamDistance = 4f;
    // The UI Panel
    public GameObject winPanel;
    public GameObject losePanel;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Vector2 deltaPos = Vector2.zero;
        if(Input.GetMouseButton(0))
        {
            Vector2 currentMousePosition = Input.mousePosition;
            if(lastMousePos == Vector2.zero)
            
                lastMousePos = currentMousePosition;
            
            deltaPos = currentMousePosition - lastMousePos;
            lastMousePos = currentMousePosition;
            Vector3 force = new Vector3(deltaPos.x, 0, deltaPos.y) * ballThrust;
            rb.AddForce(force);
            }
        else
        {
            lastMousePos = Vector2.zero;
        }
    }
    private void LateUpdate()
    {
        Vector3 position = transform.position;

        if (position.x < -wallDistance)
        {
            position.x = -wallDistance;
        }
        else if (position.x > wallDistance)
        {
            position.x = wallDistance;
        }

        //If the Camera is behind the Camera
        if (position.z < Camera.main.transform.position.z + minCamDistance)
        {
            // Block the player's position
            position.z = Camera.main.transform.position.z + minCamDistance;
        }
        // Reset the position
        transform.position = position;
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + Vector3.forward * movingSpeed * Time.fixedDeltaTime); //move ball
        Camera.main.transform.position += Vector3.forward * movingSpeed * Time.fixedDeltaTime; //move camera
    }

    IEnumerator Die(float delayTime)
    {
        // Do stuff before replaying the level 
        Debug.Log("You're dead");

        // Stop the Player from moving
        movingSpeed = 0;
        ballThrust = 0;

        // Wait some seconds
        yield return new WaitForSeconds(delayTime);

        // Do stuff after waiting some seconds

        //Replay the Level
        losePanel.SetActive(true);
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Evil")
            StartCoroutine(Die(3));
    }
    IEnumerator Win(float delayTime)
    {
        // Do stuff before waiting 
        ballThrust = 0;
        movingSpeed = 0;
        rb.velocity = Vector3.zero;

        //Wait some time 
        yield return new WaitForSeconds(delayTime);

        // Do other stuff after waiting

        // Activate the pannel
        winPanel.SetActive(true);
    }
    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Goal")
        {
            StartCoroutine(Win(0.5f));
        }
    }

}

